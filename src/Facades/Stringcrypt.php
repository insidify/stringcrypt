<?php

namespace Seamlesshr\Stringcrypt\Facades;

use Illuminate\Support\Facades\Facade;

class Stringcrypt extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'stringcrypt';
    }
}
