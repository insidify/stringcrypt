<?php

namespace Seamlesshr\Stringcrypt;

class Stringcrypt{
    static function encrypt($string){
        if(empty($string)){
            return $string;
        }

        $secret_key = env('CRY_KEY');
        $secret_iv = env('CRY_IV');
     
        $output = false;
        $encrypt_method = "AES-256-CBC";
        $key = hash( 'sha256', $secret_key );
        $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );

        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );

        return $output;
    }

    static function decrypt($string){
        if(env('APP_ENV') == 'production') {
            if(empty($string)){
                return $string;
            }

            $secret_key = env('CRY_KEY');
            $secret_iv = env('CRY_IV');
         
            $output = false;
            $encrypt_method = "AES-256-CBC";
            $key = hash( 'sha256', $secret_key );
            $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );

            $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
        } else {
            $output = $string;
        
        }
        
        return $output;
    }
}