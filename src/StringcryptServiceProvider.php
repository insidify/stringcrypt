<?php

namespace Seamlesshr\Stringcrypt;

use Illuminate\Support\ServiceProvider;

class StringcryptServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        // $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'seamlesshr');
        // $this->loadViewsFrom(__DIR__.'/../resources/views', 'seamlesshr');
        // $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        // $this->loadRoutesFrom(__DIR__.'/routes.php');

        // Publishing is only necessary when using the CLI.
        if ($this->app->runningInConsole()) {
            $this->bootForConsole();
        }
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/stringcrypt.php', 'stringcrypt');

        // Register the service the package provides.
        $this->app->singleton('stringcrypt', function ($app) {
            return new Stringcrypt;
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['stringcrypt'];
    }
    
    /**
     * Console-specific booting.
     *
     * @return void
     */
    protected function bootForConsole()
    {
        // Publishing the configuration file.
        $this->publishes([
            __DIR__.'/../config/stringcrypt.php' => config_path('stringcrypt.php'),
        ], 'stringcrypt.config');

        // Publishing the views.
        /*$this->publishes([
            __DIR__.'/../resources/views' => base_path('resources/views/vendor/seamlesshr'),
        ], 'stringcrypt.views');*/

        // Publishing assets.
        /*$this->publishes([
            __DIR__.'/../resources/assets' => public_path('vendor/seamlesshr'),
        ], 'stringcrypt.views');*/

        // Publishing the translation files.
        /*$this->publishes([
            __DIR__.'/../resources/lang' => resource_path('lang/vendor/seamlesshr'),
        ], 'stringcrypt.views');*/

        // Registering package commands.
        // $this->commands([]);
    }
}
